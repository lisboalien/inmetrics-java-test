Feature: Testing the creation, update and deletion process of an employee

  Background: Opening the browser and setting the url
    Given The user enters in "http://www.inmrobo.tk/accounts/login/"
    And He sees inmetrics logo
    And The user is logged in username "New User 2" and password "123456789"

  Scenario: Creating a new employee
    When He enters on the new employee page
    And He fills all the required fields
    And He clicks on 'Enviar' button
    Then He will see a success message of creation

  Scenario: Editing an employee
    When He enters on the "data file" employee page
    And He changes the employee hiring type to "PJ"
    Then He will see a success message of update

  Scenario: Deleting an employee
    When He searches for the employee "from data file"
    And He clicks on the 'Delete' button to delete "data file" register
    Then He will see a success message of deletion