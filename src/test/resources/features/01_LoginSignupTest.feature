Feature: Testing Signup and Login process on inmrobo website

  Background: Opening the browser and setting the url
    Given The user enters in "http://www.inmrobo.tk/accounts/login/"
    Then He sees inmetrics logo
    And He sees the login form

  Scenario: Creating a new account
    Given The user enters on the signup page
    When He creates his account with the username "New User 7" and password "123456789"
    Then He will be redirected to the Login page

  Scenario: Login in with the new account
    When He logs in with the username "New User 2" and password "123456789"
    Then He will enter on the inmetrics employees page