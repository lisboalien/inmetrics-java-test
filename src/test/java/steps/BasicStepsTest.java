package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import driver.DriverFactory;
import pages.CommonPage;
import pages.LoginPage;

public class BasicStepsTest extends DriverFactory {

    @Given("^The user enters in \"([^\"]*)\"$")
    public void url_setting(String url) {
        new LoginPage(driver).url_setting(url);
    }

    @Then("^He sees inmetrics logo$")
    public void checking_logo_element() {
        new LoginPage(driver).img_logo();
    }

    @Given("^There is a window open$")
    public void there_is_a_window_open() {
        new CommonPage(driver).there_is_a_window_open();
    }

    @Then("^Close all windows$")
    public void close_all_windows() {
        new CommonPage(driver).close_all_windows();
        destroyDriver();
    }
}
