package steps;

import archetypes.classes.Employee;
import archetypes.enums.Condition;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.DriverFactory;
import pages.EmployeeFormPage;
import pages.EmployeesPage;

import java.io.IOException;

public class EmployeesStepsTest extends DriverFactory {

    @Then("^He will enter on the inmetrics employees page$")
    public void entering_employees_page() {
        new EmployeesPage(driver).checking_employees_page();
    }

    @When("^He enters on the new employee page$")
    public void entering_new_employee_page() {
        new EmployeesPage(driver).button_new_employee(Condition.clicksOn);
        new EmployeeFormPage(driver).employee_form();
    }

    @And("^He fills all the required fields$")
    public void new_employee_creation() {
        try {
            new EmployeeFormPage(driver).employee_form_completion();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @And("^He clicks on 'Enviar' button$")
    public void sending_employee_form() {
        new EmployeeFormPage(driver).button_send(Condition.clicksOn);
    }

    @Then("^He will see a success message of (creation|update|deletion)$")
    public void success_message(String type_message) {
        new EmployeesPage(driver).alert_success(type_message);
    }

    @When("^He enters on the \"([^\"]*)\" employee page$")
    public void entering_employee_page(String name) {
        if (name.contains("data")){
            try {
                Employee e = new EmployeeFormPage(driver).get_data_employee();
                new EmployeesPage(driver).input_search(Condition.writes, e.getName());
                new EmployeesPage(driver).employee_edit_page(e.getName());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } else {
            new EmployeesPage(driver).input_search(Condition.writes, name);
            new EmployeesPage(driver).employee_edit_page(name);
        }
    }

    @And("^He changes the employee hiring type to \"([^\"]*)\"$")
    public void hiring_type_change(String hiring_type) {
        new EmployeeFormPage(driver).radio_hiring_type(Condition.clicksOn, hiring_type);
        new EmployeeFormPage(driver).button_send(Condition.clicksOn);
    }

    @When("^He searches for the employee \"([^\"]*)\"$")
    public void employee_search(String name) {
        if (name.contains("data")){
            try {
                Employee e = new EmployeeFormPage(driver).get_data_employee();
                new EmployeesPage(driver).input_search(Condition.writes, e.getName());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } else {
            new EmployeesPage(driver).input_search(Condition.writes, name);
        }
    }

    @And("^He clicks on the 'Delete' button to delete \"([^\"]*)\" register$")
    public void employee_delete(String name) {
        if (name.contains("data")){
            try {
                Employee e = new EmployeeFormPage(driver).get_data_employee();
                new EmployeesPage(driver).delete_employee(e.getName());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } else {
            new EmployeesPage(driver).delete_employee(name);
        }
    }
}
