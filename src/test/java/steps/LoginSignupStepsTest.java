package steps;

import archetypes.enums.Condition;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.DriverFactory;
import pages.LoginPage;
import pages.SignupPage;

public class LoginSignupStepsTest extends DriverFactory {

    @Given("^The user enters on the signup page$")
    public void entering_signup_page() {
        new LoginPage(driver).button_signup(Condition.clicksOn);
        new SignupPage(driver).signup_form();
    }

    @And("^He sees the login form$")
    @Then("^He will be redirected to the Login page$")
    public void checking_login_form() {
        new LoginPage(driver).login_form();
    }

    @When("^He creates his account with the username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void creating_new_account(String username, String password) {
        new SignupPage(driver).account_creation(username, password);
    }

    @When("^He logs in with the username \"([^\"]*)\" and password \"([^\"]*)\"$")
    @Given("^The user is logged in username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void complete_login(String username, String password) throws Throwable {
        new LoginPage(driver).complete_login(username, password);
    }
}
