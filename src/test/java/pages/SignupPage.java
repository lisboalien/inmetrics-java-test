package pages;

import archetypes.enums.Condition;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignupPage {
    private WebDriver driver;
    /*------------------------WebElements----------------------*/
    @FindBy(className = "login100-form-title")
    private WebElement signUpFormTitleLocator;

    @FindBy(xpath = "//form/div[@class='p-t-10 p-b-1']/span")
    private WebElement userNameInputLabelLocator;

    @FindBy(name = "username")
    private WebElement userNameInputLocator;

    @FindBy(xpath = "//form/div[@class='p-t-13 p-b-1'][1]/span")
    private WebElement passwordInputLabelLocator;

    @FindBy(name = "pass")
    private WebElement passwordInputLocator;

    @FindBy(xpath = "//form/div[@class='p-t-13 p-b-1'][2]/span")
    private WebElement confirmPasswordInputLabelLocator;

    @FindBy(name = "confirmpass")
    private WebElement confirmPasswordInputLocator;

    @FindBy(className = "login100-form-btn")
    private WebElement signupButtonLocator;

    @FindBy(xpath = "//div/a[@href='/accounts/login/']")
    private WebElement loginFormButtonLocator;

    /**
     * Constructor of the SignupPage class
     *
     * @param driver (the actual driver)
     */
    public SignupPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /*------------------------Components-----------------------*/

    /**
     * Method to deal with username input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the username string and send the keys to the component)
     * @param username  (String username)
     */
    public void input_username(Condition condition, String... username) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The username label is incorrect", "Usuário", userNameInputLabelLocator.getText());
            Assert.assertTrue("The username input is not displayed", userNameInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            userNameInputLocator.sendKeys(username);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with password input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the password string and send the keys to the component)
     * @param password  (String username)
     */
    public void input_password(Condition condition, String... password) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The password label is incorrect", "Senha", passwordInputLabelLocator.getText());
            Assert.assertTrue("The password input is not displayed", passwordInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            passwordInputLocator.sendKeys(password);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with confirm password input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the password string and send the keys to the component)
     * @param password  (String username)
     */
    public void input_confirm_password(Condition condition, String... password) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The confirm password label is incorrect", "Confirme a senha", confirmPasswordInputLabelLocator.getText());
            Assert.assertTrue("The confirm password input is not diplayed", confirmPasswordInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            confirmPasswordInputLocator.sendKeys(password);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Login button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_login(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The login button label is incorrect", "Cadastrar", signupButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            signupButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Signup button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_signup(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The signup button label is incorrect", "Login", loginFormButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            loginFormButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /*------------------------Assert and Action Methods-----------------------*/

    /**
     * Method to assert that the signup form is correct
     */
    public void signup_form() {
        Assert.assertEquals("The form text is incorrect", "Cadastre-se", signUpFormTitleLocator.getText());
        input_username(Condition.sees);
        input_password(Condition.sees);
        input_confirm_password(Condition.sees);
        button_signup(Condition.sees);
        button_login(Condition.sees);
    }

    /**
     * Method that completes an account creation
     *
     * @param username (String username)
     * @param password (String password)
     */
    public void account_creation(String username, String password) {
        input_username(Condition.writes, username);
        input_password(Condition.writes, password);
        input_confirm_password(Condition.writes, password);
        signupButtonLocator.click();

    }
}
