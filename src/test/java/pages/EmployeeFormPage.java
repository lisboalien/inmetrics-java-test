package pages;

import archetypes.classes.ComponentList;
import archetypes.classes.Employee;
import archetypes.enums.Condition;
import archetypes.enums.Lists;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class EmployeeFormPage {
    private WebDriver driver;

    /*------------------------WebElements----------------------*/
    @FindBy(xpath = "//div/input[@id='inputNome']/preceding::div[1]/span")
    private WebElement nameInputLabelLocator;

    @FindBy(id = "inputNome")
    private WebElement nameInputLocator;

    @FindBy(xpath = "//div/input[@id='cpf']/preceding::div[1]/span")
    private WebElement cpfInputLabelLocator;

    @FindBy(id = "cpf")
    private WebElement cpfInputLocator;

    @FindBy(xpath = "//div/select[@id='slctSexo']/preceding::div[1]/span")
    private WebElement sexSelectBoxLabelLocator;

    @FindBy(id = "slctSexo")
    private WebElement sexSelectBoxLocator;

    @FindBy(xpath = "//div/input[@id='inputAdmissao']/preceding::div[1]/span")
    private WebElement admissionDateInputLabelLocator;

    @FindBy(id = "inputAdmissao")
    private WebElement admissionDateInputLocator;

    @FindBy(xpath = "//div/input[@id='inputCargo']/preceding::div[1]/span")
    private WebElement postInputLabelLocator;

    @FindBy(id = "inputCargo")
    private WebElement postInputLocator;

    @FindBy(xpath = "//div/input[@id='dinheiro']/preceding::div[1]/span")
    private WebElement salaryInputLabelLocator;

    @FindBy(id = "dinheiro")
    private WebElement salaryInputLocator;

    @FindBy(xpath = "//div[@class='radio-button']/preceding::div[1]/span")
    private WebElement hiringTypeRadioLabelLocator;

    @FindBy(xpath = "//div[@class='radio-button']/input")
    private WebElement hiringTypeRadioListLocator;

    @FindBy(className = "cadastrar-form-btn")
    private WebElement sendFormButtonLocator;

    @FindBy(className = "cancelar-form-btn")
    private WebElement cancelFormButtonLocator;

    private final ComponentList sexSelectBoxList = new ComponentList();

    private final ComponentList hiringTypeRadioList = new ComponentList();

    /*------------------------WebElements----------------------*/

    /**
     * Constructor of the EmployeesPage class
     *
     * @param driver (the actual driver)
     */
    public EmployeeFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Method that sets the sex select box component
     */
    public void set_sex_select_box_list() {
        sexSelectBoxList.setComponentList("//select[@id='slctSexo']/option", Lists.SelectBox, driver);
    }

    /**
     * Method that sets the hiring type radio options
     */
    public void set_hiring_type_radio_list() {
        hiringTypeRadioList.setComponentList("//input[@name='tipo-contratacao']", Lists.RadioOptions, driver);
    }

    /**
     * Method to deal with name input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the name string and send the keys to the component)
     * @param name      (String name)
     */
    public void input_name(Condition condition, String... name) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The name label is incorrect", "Nome", nameInputLabelLocator.getText());
            Assert.assertTrue("The name input is not displayed", nameInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            nameInputLocator.sendKeys(name);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }


    /**
     * Method to deal with cpf input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the cpf string and send the keys to the component)
     * @param cpf       (String cpf)
     */
    public void input_cpf(Condition condition, String... cpf) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The cpf label is incorrect", "CPF", cpfInputLabelLocator.getText());
            Assert.assertTrue("The cpf input is not displayed", cpfInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            cpfInputLocator.sendKeys(cpf);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with sex input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> clicks on the option that matches the string)
     * @param sex       (String sex)
     */
    public void select_box_sex(Condition condition, String... sex) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The sex label is incorrect", "Sexo", sexSelectBoxLabelLocator.getText());
            Assert.assertTrue("The sex input is not displayed", sexSelectBoxLocator.isDisplayed());
        } else if (condition.equals(Condition.clicksOn)) {
            set_sex_select_box_list();
            sexSelectBoxList.getComponentElement(sex[0]).click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with admission date input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the admission string and send the keys to the component)
     * @param admission (String admission date)
     */
    public void input_admission_date(Condition condition, String... admission) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The admission date label is incorrect", "Admissão", admissionDateInputLabelLocator.getText());
            Assert.assertTrue("The admission date input is not displayed", admissionDateInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            admissionDateInputLocator.sendKeys(admission);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }


    /**
     * Method to deal with post input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the post string and send the keys to the component)
     * @param post      (String admission date)
     */
    public void input_post(Condition condition, String... post) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The post label is incorrect", "Cargo", postInputLabelLocator.getText());
            Assert.assertTrue("The post input is not displayed", postInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            postInputLocator.sendKeys(post);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with salary input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the salary string and send the keys to the component)
     * @param salary    (String admission date)
     */
    public void input_salary(Condition condition, String... salary) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The salary label is incorrect", "Salário", salaryInputLabelLocator.getText());
            Assert.assertTrue("The salary input is not displayed", salaryInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            salaryInputLocator.sendKeys(salary);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with hiring type radio component
     *
     * @param condition   (Condition enum,
     *                    sees -> Asserts the characteristics of the component
     *                    clicksOn -> clicks on the option that matches the string)
     * @param hiring_type (String admission date)
     */
    public void radio_hiring_type(Condition condition, String... hiring_type) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The hiring type label is incorrect", "Tipo de Contratação", hiringTypeRadioLabelLocator.getText());
            Assert.assertTrue("The hiring type input is not displayed", hiringTypeRadioListLocator.isDisplayed());
        } else if (condition.equals(Condition.clicksOn)) {
            set_hiring_type_radio_list();
            hiringTypeRadioList.getComponentElement(hiring_type[0]).click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Send button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_send(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertTrue("The send button is not displayed", sendFormButtonLocator.isDisplayed());
        } else if (condition.equals(Condition.clicksOn)) {
            sendFormButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Cancel button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_cancel(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertTrue("The cancel button is not displayed", cancelFormButtonLocator.isDisplayed());
        } else if (condition.equals(Condition.clicksOn)) {
            cancelFormButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }


    /*------------------------Assert and Action Methods-----------------------*/

    /**
     * Method to assert that the employee form is correct
     */
    public void employee_form() {
        input_name(Condition.sees);
        input_cpf(Condition.sees);
        select_box_sex(Condition.sees);
        input_admission_date(Condition.sees);
        input_post(Condition.sees);
        input_salary(Condition.sees);
        radio_hiring_type(Condition.sees);
        button_send(Condition.sees);
        button_cancel(Condition.sees);
    }

    /**
     * Method to fill the employee with data
     */
    public void employee_form_completion() throws IOException {
        Employee employee = get_data_employee();

        input_name(Condition.writes, employee.getName());
        input_cpf(Condition.writes, employee.getCpf());
        select_box_sex(Condition.clicksOn, employee.getSex());
        input_admission_date(Condition.writes, employee.getAdmissionDate());
        input_post(Condition.writes, employee.getPost());
        input_salary(Condition.writes, employee.getSalary());
        radio_hiring_type(Condition.clicksOn, employee.getHiringType());
    }

    /**
     * Method to get the employee data from employee.yaml
     *
     * @return (Employee employee)
     * @throws IOException
     */
    public Employee get_data_employee() throws IOException {
        // Getting the data from the yaml file
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.findAndRegisterModules();

        return om.readValue(new File("src/test/resources/data/employee.yaml"), Employee.class);
    }
}
