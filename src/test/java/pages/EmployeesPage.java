package pages;

import archetypes.classes.ComponentList;
import archetypes.enums.Condition;
import archetypes.enums.Lists;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeesPage {
    private WebDriver driver;

    /*------------------------WebElements----------------------*/

    @FindBy(xpath = "//a[@href='/empregados/']")
    private WebElement employeesButtonLocator;

    @FindBy(xpath = "//a[@href='/empregados/new_empregado']")
    private WebElement newEmployeeButtonLocator;

    @FindBy(xpath = "//a[contains(@href,'logout')]")
    private WebElement logoutButtonLocator;

    @FindBy(css = ".dataTables_filter>label")
    private WebElement filterInputLabelLocator;

    @FindBy(css = ".dataTables_filter>label>input")
    private WebElement filterInputLocator;

    @FindBy(id = "tabela")
    private WebElement employeesTableLocator;

    @FindBy(className = "alert-success")
    private WebElement alertSuccessLocator;

    private final ComponentList employeesList = new ComponentList();
    /*------------------------WebElements----------------------*/


    /**
     * Constructor of the EmployeesPage class
     *
     * @param driver (the actual driver)
     */
    public EmployeesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /*------------------------Components-----------------------*/

    /**
     * Method to set the employees list
     */
    public void set_employees_list() {
        employeesList.setComponentList("//table[@id='tabela']/tbody/tr", Lists.EmployeesList, driver);
    }

    /**
     * Method to deal with Employees button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_employees(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The employees button label is incorrect", "FUNCIONÁRIOS", employeesButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            employeesButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with New Employee button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_new_employee(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The new employee button label is incorrect", "NOVO FUNCIONÁRIO", newEmployeeButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            newEmployeeButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Logout button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_logout(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The logout button label is incorrect", "SAIR", logoutButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            logoutButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with search input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the username string and send the keys to the component)
     * @param password  (String username)
     */
    public void input_search(Condition condition, String... password) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The search label is incorrect", "Pesquisar:", filterInputLabelLocator.getText());
            Assert.assertTrue("The search input is not displayed", filterInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            filterInputLocator.sendKeys(password);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to assert if the success message is correct
     */
    public void alert_success(String type_message) {
        if (type_message.equals("creation")) {
            Assert.assertEquals("The success message is incorrect", "SUCESSO! Usuário cadastrado com sucesso\n" +
                    "×", alertSuccessLocator.getText());
        } else if (type_message.equals("update")) {
            Assert.assertEquals("The success message is incorrect", "SUCESSO! Informações atualizadas com sucesso\n" +
                    "×", alertSuccessLocator.getText());
        } else if (type_message.equals("deletion")) {
            Assert.assertEquals("The success message is incorrect", "SUCESSO! Funcionário removido com sucesso\n" +
                    "×", alertSuccessLocator.getText());
        } else {
            System.err.println("This component does not support the message type " + type_message);
        }
    }

    /*------------------------Assert and Action Methods-----------------------*/

    /**
     * Method to assert the employees page is displaying the basic components correctly
     */
    public void checking_employees_page() {
        button_employees(Condition.sees);
        button_new_employee(Condition.sees);
        button_logout(Condition.sees);
        input_search(Condition.sees);
        Assert.assertTrue("The Employee table list is not displayed", employeesTableLocator.isDisplayed());
    }

    /**
     * Method to enter an employee page of the list
     *
     * @param name (String name)
     */
    public void employee_edit_page(String name) {
        set_employees_list();
        // The edit button is the second button of the action button list
        employeesList.getComponentElementActionButtons(name).get(1).click();
    }

    /**
     * Method to delete an employee
     *
     * @param name (String name)
     */
    public void delete_employee(String name) {
        set_employees_list();
        // The delete button is the first button of the action button list
        employeesList.getComponentElementActionButtons(name).get(0).click();
    }
}
