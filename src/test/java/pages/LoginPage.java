package pages;

import archetypes.enums.Condition;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;
    /*------------------------WebElements----------------------*/
    @FindBy(xpath = "//div[@class='container-fluid']/a/img")
    private WebElement inmetricsLogoLocator;

    @FindBy(className = "login100-form-title")
    private WebElement loginFormTitleLocator;

    @FindBy(xpath = "//form/div[@class='p-t-20 p-b-9']/span")
    private WebElement userNameInputLabelLocator;

    @FindBy(name = "username")
    private WebElement userNameInputLocator;

    @FindBy(xpath = "//form/div[@class='p-t-13 p-b-9']/span")
    private WebElement passwordInputLabelLocator;

    @FindBy(name = "pass")
    private WebElement passwordInputLocator;

    @FindBy(className = "login100-form-btn")
    private WebElement loginButtonLocator;

    @FindBy(xpath = "//div/a[@href='/accounts/signup/']")
    private WebElement signUpFormButtonLocator;

    /**
     * Constructor of the LoginPage class
     *
     * @param driver (the actual driver)
     */
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /*------------------------Components-----------------------*/

    /**
     * Method that opens a URL on the configured browser
     *
     * @param url (String URL)
     */
    public void url_setting(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }

    /**
     * Method that asserts the presence of the logo image
     */
    public void img_logo() {
        Assert.assertTrue("The logo is not displayed.", inmetricsLogoLocator.isDisplayed());
    }

    /**
     * Method to deal with username input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the username string and send the keys to the component)
     * @param username  (String username)
     */
    public void input_username(Condition condition, String... username) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The username label is incorrect", "Usuário", userNameInputLabelLocator.getText());
            Assert.assertTrue("The username input is not diplayed", userNameInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            userNameInputLocator.sendKeys(username);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with password input component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  writes -> takes the username string and send the keys to the component)
     * @param password  (String username)
     */
    public void input_password(Condition condition, String... password) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The password label is incorrect", "Senha", passwordInputLabelLocator.getText());
            Assert.assertTrue("The password input is not diplayed", passwordInputLocator.isDisplayed());
        } else if (condition.equals(Condition.writes)) {
            passwordInputLocator.sendKeys(password);
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Login button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_login(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The login button label is incorrect", "Entre", loginButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            loginButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /**
     * Method to deal with Signup button component
     *
     * @param condition (Condition enum,
     *                  sees -> Asserts the characteristics of the component
     *                  clicksOn -> Clicks on the component)
     */
    public void button_signup(Condition condition) {
        if (condition.equals(Condition.sees)) {
            Assert.assertEquals("The signup button label is incorrect", "Cadastre-se", signUpFormButtonLocator.getText());
        } else if (condition.equals(Condition.clicksOn)) {
            signUpFormButtonLocator.click();
        } else {
            System.err.println("This component does not support the condition " + condition.toString());
        }
    }

    /*------------------------Assert and Action Methods-----------------------*/

    /**
     * Method to assert that the login for is correct
     */
    public void login_form() {
        Assert.assertEquals("The form text is incorrect", "Login", loginFormTitleLocator.getText());
        input_username(Condition.sees);
        input_password(Condition.sees);
        button_login(Condition.sees);
        button_signup(Condition.sees);
    }

    /**
     * Method that does a complete login process
     *
     * @param username (String username)
     * @param password (String password)
     */
    public void complete_login(String username, String password) {
        input_username(Condition.writes, username);
        input_password(Condition.writes, password);
        button_login(Condition.clicksOn);
    }
}
