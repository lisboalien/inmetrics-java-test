package archetypes.classes;

/**
 * Created by Aline Lisboa on 19/12/2020.
 * Class that defines an Employee
 */
public class Employee {
    private String name;
    private String cpf;
    private String sex;
    private String admissionDate;
    private String post;
    private String salary;
    private String hiringType;

    public Employee(String name, String cpf, String sex, String admissionDate, String post, String salary, String hiringType) {
        this.name = name;
        this.cpf = cpf;
        this.sex = sex;
        this.admissionDate = admissionDate;
        this.post = post;
        this.salary = salary;
        this.hiringType = hiringType;
    }

    // Without a default constructor, Jackson will throw an exception
    public Employee() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(String admissionDate) {
        this.admissionDate = admissionDate;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getHiringType() {
        return hiringType;
    }

    public void setHiringType(String hiringType) {
        this.hiringType = hiringType;
    }
}
