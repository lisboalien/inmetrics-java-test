package archetypes.classes;

import archetypes.enums.Lists;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Aline Lisboa on 19/12/2020.
 * Class that defines a Component List and it extends
 */
public class ComponentList {
    private WebDriver driver;
    private String xpath;
    private Lists type;
    private String xpathType;
    private String xpathPagination;
    private List<WebElement> componentList;
    private List<WebElement> paginationList;

    /**
     * Method that returns the component list
     *
     * @return (List of WebElements)
     */
    public List<WebElement> getComponentList() {
        return componentList;
    }

    /**
     * This method set the component list and the variables that composes it
     *
     * @param formXpath (xpath locator of the list)
     * @param type      (type of the list)
     * @param driver    (WebDriver)
     */
    public void setComponentList(String formXpath, Lists type, WebDriver driver) {
        this.driver = driver;
        setXpath(formXpath);
        setType(type);
        setXpathType();
        setXpathPagination();
        this.componentList = driver.findElements(By.xpath(getXpath()));

        // Setting the pagination list if the list has the pagination element
        if (!(xpathPagination.equals(""))) {
            this.paginationList = driver.findElements(By.xpath(getXpathPagination()));
        }
    }

    /**
     * This method returns a specific element of the list
     * There is an Assert on the catch to show the error in the test report
     *
     * @param name (The name of the element you want to return)
     * @return WebElement of the list
     */
    public WebElement getComponentElement(String name) {
        List<WebElement> componentList = getComponentList();
        WebElement componentElement = null;
        try {
            for (int t = 0; t < getPaginationSize(); t++) {
                for (int i = 0; i < componentList.size(); i++) {
                    if (type.equals(Lists.RadioOptions)) {
                        if (name.equalsIgnoreCase(this.componentList.get(i).getAttribute("value"))) {
                            componentElement = this.componentList.get(i).findElement(By.xpath(getXpath() + "[" + (i + 1) + "]" + getXpathType()));
                            break;
                        }
                    } else {
                        if (name.equals(this.componentList.get(i).getText())) {
                            componentElement = this.componentList.get(i).findElement(By.xpath(getXpath() + "[" + (i + 1) + "]" + getXpathType()));
                            break;
                        }
                    }
                }
                if (componentElement == null) {
                    throw new IndexOutOfBoundsException();
                }
            }
        } catch (IndexOutOfBoundsException err) {
            System.out.println(err);
            Assert.fail("The component with value '" + name + "' was not found in " + getType());
        }
        return componentElement;
    }

    /**
     * The method returns a list of action button elements of an element of the list
     *
     * @param name (The name of the element you want to return)
     * @return List<WebElement> action buttons of an element of the list
     */
    public List<WebElement> getComponentElementActionButtons(String name) {
        List<WebElement> componentList = getComponentList();
        WebElement componentElementTemp;
        List<WebElement> componentElements = null;
        try {
            for (int t = 0; t < getPaginationSize(); t++) {
                for (int i = 0; i < componentList.size(); i++) {
                    if (type.equals(Lists.EmployeesList)) {
                        componentElementTemp = this.componentList.get(i).findElement(By.xpath(getXpathType()));
                        if (name.equals(componentElementTemp.getText())) {
                            componentElements = this.componentList.get(i).findElements(By.xpath(getXpath() + "[" + (i + 1) + "]//button"));
                            break;
                        }
                    } else {
                        if (name.equals(this.componentList.get(i).getText())) {
                            componentElements = this.componentList.get(i).findElements(By.xpath(getXpath() + "[" + (i + 1) + "]" + getXpathType()));
                            break;
                        }
                    }
                }
                if (componentElements == null && t < getPaginationSize() - 1) {
                    /*Click on next page*/
                    paginationList.get(t + 1).click();
                    /*Reload the component list*/
                    this.componentList = driver.findElements(By.xpath(getXpath()));
                } else if (componentElements != null) {
                    break;
                }
            }
            if (componentElements == null) {
                throw new IndexOutOfBoundsException();
            }
        } catch (IndexOutOfBoundsException err) {
            System.out.println(err);
            Assert.fail("The component with value '" + name + "' or the action buttons were not found in " + getType());
        }
        return componentElements;

    }

    /**
     * Method that returns the component list xpath
     *
     * @return (String xpath)
     */
    public String getXpath() {
        return xpath;
    }

    /**
     * Method that sets the component list xpath
     *
     * @param xpath (String xpath)
     */
    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    /**
     * This method returns the list type
     *
     * @return (String list type)
     */
    public Lists getType() {
        return type;
    }

    /**
     * This method sets the list type. It can be an enum.
     *
     * @param type (String list type)
     */
    public void setType(Lists type) {
        this.type = type;
    }

    /**
     * This method returns the xpath ending based on the list type
     *
     * @return (String xpath)
     */
    public String getXpathType() {
        return xpathType;
    }

    /**
     * This method set xpath ending based on component list type
     * You can select and set these xpath endings based on the system you are testing.
     */
    public void setXpathType() {
        if (type == Lists.EmployeesList) {
            xpathType = ".//td[1]";
        } else {
            xpathType = "";
        }
    }

    /**
     * This method returns the pagination xpath based on the list type
     *
     * @return (String xpath)
     */
    public String getXpathPagination() {
        return xpathPagination;
    }

    /**
     * This method sets the xpath of the pagination element of the list
     * This locator depends on the system and list you are testing
     */
    public void setXpathPagination() {
        if (type == Lists.EmployeesList) {
            xpathPagination = "//a[@data-dt-idx>=1and@data-dt-idx<7]";
        } else {
            xpathPagination = "";
        }
    }

    /**
     * Method to return pagination size. If the list does not have a pagination the return value will be always 1
     *
     * @return int pagination.size()
     */
    private int getPaginationSize() {
        if (getType().equals(Lists.EmployeesList) && paginationList.size() > 0) {
            return paginationList.size();
        } else {
            return 1;
        }
    }

    /**
     * Method that gets the size of the component list
     *
     * @return (int componentList.size)
     */
    public int getListSize() {
        return componentList.size();
    }
}
