package archetypes.enums;

public enum Condition {
    writes, clicksOn, sees
}
