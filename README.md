# Teste Inmetrics - Descrição de como executar o projeto

Projeto de teste para avaliação pela InMetrics em Java

# Instruções de Setup

## Java Setup

Este projeto precisa da versão do Java 1.8

## Setup de Webdriver

Instalar as últimas versões do
[Google Chrome](https://www.google.com/chrome/)
e [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/).

Você também vai precisar das últimas versões dos webdrivers executáveis para esses browsers: [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/) para o Chrome
e [geckodriver](https://github.com/mozilla/geckodriver/releases) para o Firefox.

O ChromeDriver e gheckodriver devem ser instalados no PATH do sistema [system path](https://en.wikipedia.org/wiki/PATH_(variable)).

## Setup do projeto

1. Clone este repositório
2. Abra o arquivo `src/config.properties` e altere os caminhos para os executáveis do Chrome e Pathway para os caminhos onde estão no seu computador

### Para executar os testes via linha de comando

3. Acesse a pasta que clonou este repositório via terminal e execute o seguinte comando
`mvn test`
   
### Para executar via IDE (Intellij)

3. Importe o projeto 
4. Abra a classe `src/test/java/steps/RunCucumberTest.java`
5. Clique com o botão direito e clique na opção `run 'RunCucumberTest'` ou pressione `ctrl+shift+f10`

## Report

O relatório de execução do teste fica em `target/cucumber-reports`

## Sobre os testes

A feature `01_LoginSignupTest.feature` contempla os testes de criação de conta e login
O cenário de criação de conta deve ser atualizado a cada execução para passar com sucesso, pois a conta já existirá numa segunda execução

A feature `02_CompleteEmployeeRegisterTest.feature` comtempla os testes de criação, edição e deleção de um funcionário.
Os dados do funcionário estão dentro do arquivo `src/test/resources/data/employee.yaml`

## Sobre o Autor

Este projeto foi feito por **Aline de Farias Lisboa**.

* LinkedIn: [Aline Lisboa](https://www.linkedin.com/in/alinelisboa/)